[[!CommonTools? &cmd=`loadChunk` &name=`doctype`]]
<head>
	[[!CommonTools? &cmd=`loadChunk` &name=`meta`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`linkrel`]]
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsTop`]]
</head>
<body>
	<div class="container">
		[[!CommonTools? &cmd=`loadChunk` &name=`header`]]
		<div class="row">
			<aside class="col-xs-4">
				Sidebar
				[[!CommonTools? &cmd=`loadChunk` &name=`articleSidebar`]] 
			</aside>
			<main id="content" role="main" class="col-xs-8" itemscope itemtype="http://schema.org/WebPage">
				<h1 itemprop="name">[[*pagetitle]]</h1>
                                <div itemprop="mainContentOfPage">[[*content]]</div>
			</main>
		</div><!--/.row -->
		[[!CommonTools? &cmd=`loadChunk` &name=`footer`]] 
	</div><!-- /container -->
	[[!CommonTools? &cmd=`loadChunk` &name=`scriptsBottom`]]  
</body>
</html>