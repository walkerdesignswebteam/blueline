<?php
/*
WALKER DESIGNS COPYRIGHT
------------------------
This code is copyright by Walker Designs. Walker Designs grants you licence to use this
code on your website. You are not licenced to share or modify this code or allow access
to this code to external parties.
*/
$s_ret='';
if(isset($cmd)===true){
	
	//as this may be called many times.. only require and declare once.
	require_once('core/components/commontools/CommonTools.class.php');
	$o_CommonTools = new CommonTools($modx);
	
	//main switch and param verification
	switch($cmd){
                case 'breadCrumbs':
                        if(!isset($show_home)){
                            $show_home=1;
                        }
                        $s_ret=$o_CommonTools->breadCrumbs($sep,$show_home);
			break;
		case 'loadTemplate':
			if(isset($json_options)===false){$json_options='';}
			$s_ret=$o_CommonTools->loadTemplate($name,$json_options);
			break;
		case 'loadChunk':
			if(isset($json_options)===false){$json_options='';}
			$s_ret=$o_CommonTools->loadChunk($name,$json_options);
			break;	
		case 'parentIds':
			$s_ret=$o_CommonTools->parentIds($pageID);
			break;	
		case 'smartCache':
			$s_ret=$o_CommonTools->smartCache($snippet,$args);
			break;	
		case 'getHttpType':
			$s_ret=$o_CommonTools->getHttpType();
			break;		
		case 'getNextChild':
			if(isset($parentID,$pageID)===true){
				$s_ret=$o_CommonTools->getNextChild($pageID,$parentID);
			}else{
				$s_ret='Variable parentID or pageID not set.';
			}
			break;
		case 'getChildIndex':
			if(empty($orderField)===true){
				$orderField='menuindex';
			}
			if(empty($orderDirection)===true){
				$orderDirection='ASC';
			}
			if(isset($parentID,$pageID)===true){
				$s_ret=$o_CommonTools->getChildIndex($parentID, $pageID, $orderField, $orderDirection);
			}else{
				$s_ret='Variable parentID or pageID not set.';
			}
			break;
		case 'getTemplateVar':
			if(isset($tvName,$pageID)===true){
				$s_ret=$o_CommonTools->getTemplateVar($tvName, $pageID);
			}else{
				$s_ret='Variable tvName or pageID not set.';
			}
			break;
		case 'getDocumentIDsWithTemplate':
			if(isset($template)===true){
				$s_ret=$o_CommonTools->getDocumentIDsWithTemplate($template);
			}else{
				$s_ret='Variable template not set.';
			}
			break;
		case 'stringTrim':
			if(isset($string,$maxChars)===true){
				if(isset($suffixIfCut)===false){
					$suffixIfCut='…';
				}
				$s_ret=$o_CommonTools->stringTrim($string, $maxChars, $suffixIfCut);
			}else{
				$s_ret='Variable string or maxChars not set.';
			}
			break;
		case 'dateFormat':
			if(isset($formatString)===true){
				if(isset($timestamp)===false){
					$timestamp=NULL;
				}
				$s_ret=$o_CommonTools->dateFormat($formatString,$timestamp);
			}else{
				$s_ret='Variable formatString not set.';
			}
			break;
		case 'menuFromLevel':
			if(isset($topLevel,$maxMenuDepth)===true){
				$s_ret=$o_CommonTools->menuFromLevel($topLevel,$maxMenuDepth);
			}else{
				$s_ret='Variable topLevel or maxMenuDepth not set.';
			}
			break;
		case 'pageProperty':
			if(isset($property)===true){
				if(isset($pageID)===false){
					$pageID=NULL;
				}
				$s_ret=$o_CommonTools->pageProperty($property,$pageID);
			}else{
				$s_ret='Variable property not set.';
			}
			break;
		case 'redirectToDocument':
			if(isset($pageID)===true){
				$s_ret=$o_CommonTools->redirectToDocument($pageID);
			}else{
				$s_ret='Variable pageID not set.';
			}
			break;	
		case 'searchResultProduct':
			if(isset($id,$idx,$extract,$img_tv,$img_width,$img_height)===true){
				$s_ret=$o_CommonTools->searchResultProduct($id,$idx,$extract,$img_tv,$img_width,$img_height);
			}else{
				$s_ret='Variable id,idx,extract,img_tv,img_width or img_height not set.';
			}
			break;
		case 'hardcodeLink':
			if(isset($pageID)===true){
				$s_ret=$o_CommonTools->hardcodeLink($pageID,$titleField);
			}else{
				$s_ret='Variable pageID not set.';
			}
			break;
		case 'hardcodeMenu':
			if(isset($pageIDs)===true){
				$s_ret=$o_CommonTools->hardcodeMenu($pageIDs,$titleField);
			}else{
				$s_ret='Variable pageIDs not set.';
			}
			break;
		case 'facebook_shareLink':
			if(isset($pageID,$class)===true){
				if(isset($linkText)===false){
					$linkText='';
				}
				$s_ret=$o_CommonTools->facebook_shareLink($pageID,$class,$linkText);
			}else{
				$s_ret='Variable pageID or class not set.';
			}
			break;
		case 'redirectToFirstChild':
			if(isset($orderField)===false){
				$orderField='menuindex';
			}
			if(isset($orderDirection)===false){
				$orderDirection='ASC';
			}
			$o_CommonTools->redirectToFirstChild($orderField,$orderDirection);
			break;
		case 'stringRawUrlEncode':
			if(isset($string)===true){
				$s_ret=$o_CommonTools->stringRawUrlEncode($string);
			}else{
				$s_ret='Variable string or class not set.';
			}
			break;
		case 'nextPrevLinks':
			if(!isset($orderBy)){
			    $orderBy = 'menuindex'; // default in NewsListing-snippet
			}			
			if(!isset($sortOrder)){
			    $sortOrder = 'ASC'; // 'ASC' or 'DESC'
			}
			if(!isset($displayTitle) || $displayTitle==1){
			    $displayTitle = true; //Show 'prev' and 'next' (false) or display document titles (true)?
			}else{
				$displayTitle = false;
			}
			if(!isset($nextText)){
			    $nextText='Next &gt;';
			}
			if(!isset($prevText)){
			    $prevText='&lt; Previous';
			}
			if(!isset($rotate) || $rotate==1){
				$rotate=true;
			}else{
				$rotate=false;
			}
			$s_ret=$o_CommonTools->nextPrevLinks($orderBy,$sortOrder,$displayTitle,$nextText,$prevText,$rotate);
			break;
		case 'gallery_directoryThumbLinks':
			if(!isset($thumbWidth)){
			    $thumbWidth=150;
			}
			if(!isset($thumbHeight)){
			    $thumbHeight=150;
			}
			if(!isset($wrapperStart)){
			    $wrapperStart='<div>';
			}
			if(!isset($wrapperEnd)){
			    $wrapperEnd='</div>';
			}
			if(!isset($anchorAttributes)){
			    $anchorAttributes='';
			}
			if(isset($path)===true){
				$s_ret=$o_CommonTools->gallery_directoryThumbLinks($path,$thumbWidth,$thumbHeight,$wrapperStart,$wrapperEnd,$anchorAttributes);
			}else{
				$s_ret='Variable path not set.';
			}
			break;
		case 'debug_processTime':
			if(!isset($processTrackId)){
			    $processTrackId='sjng902438t';
			}
			if(!isset($note)){
			    $note='';
			}
			if(!isset($output) && $output==1){
			    $output=true;
			}else{
				$output=false;
			}
			$s_ret=$o_CommonTools->debug_processTime($processTrackId,$note,$output);
			break;
		case 'user_isloggedIn':
			$s_ret=$o_CommonTools->user_isloggedIn();
			break;
		case 'getFirstChildWithChildren':
			if(isset($orderField)===false){
				$orderField='menuindex';
			}
			if(isset($orderDirection)===false){
				$orderDirection='ASC';
			}
			if(isset($pageID)===true){
				$s_ret= $o_CommonTools->getFirstChildWithChildren($pageID,$orderField,$orderDirection);
			}else{
				$s_ret='Variable pageID not set.';
			}
			break;
		case 'getRequestVar':
			if(isset($varType)===false){
				$s_ret='getRequestVar var type not set';
			}else{
				if($varType!='request' && $varType!='post' && $varType!='get'){
					$s_ret='getRequestVar - varType must be "request","get" or "post"';
				}else{
					if(isset($varName,$varType)===true){
						if(isset($default)===false){
							$default='';
						}
						$s_ret= $o_CommonTools->getRequestVar($varName,$varType,$default);
					}else{
						$s_ret='Variable pageID not set.';
					}
				}
			}
			break;
		case 'getDocumentsWithTvNumberRange':
			if(isset($priceLow,$priceHigh,$templateVar)===false){
				$s_ret='Need to set priceLow, priceHigh, templateVar attributes in snippet call.';
			}else{
				$s_ret=$o_CommonTools->getDocumentsWithTvNumberRange($priceLow,$priceHigh,$templateVar);
			}
			break;
		case 'getDocumentsWithTvValue':
			if($prefixNeg=='1'){
				$b_prefixNeg=true;
			}else{
				$b_prefixNeg=false;
			}
			if(isset($value,$templateVar)===false){
				$s_ret='Need to set value, templateVar attributes in snippet call.';
			}else{
				$s_ret=$o_CommonTools->getDocumentsWithTvValue($value,$templateVar,$b_prefixNeg);
			}
			break;
		case 'convertExcludeDocsToGetResourcesExclude':
			if(isset($idString)===false){
				$s_ret='Need to set idString attribute in snippet call.';
			}else{
				$s_ret=$o_CommonTools->convertExcludeDocsToGetResourcesExclude($idString);
			}
			break;
		case 'isMobileDevice':
			$s_ret=$o_CommonTools->isMobileDevice();
			break;
		/*
		//Snippet alwasy returns a string. so need to include the commonTools and call method directly.
		case 'getDocChildren':
			if(isset($orderField)===false){$orderField='menuindex';}
			if(isset($orderDirection)===false){$orderDirection='ASC';}
			if(isset($pageID)===false){
				$s_ret='Need pageID for getDocChildren';
			}else{
				$s_ret=$o_CommonTools->getDocChildren($pageID, $orderField, $orderDirection);
			}
			break;
		 */
                case 'noRobotsForHttps':
                    $s_ret=$o_CommonTools->noRobotsForHttps();
                    break;
                case 'cannonicalURL':
                    $s_ret=$o_CommonTools->cannonicalURL();
                    break;
                case 'URLNoSlash':
                    $s_ret=$o_CommonTools->URLNoSlash();
                    break;
		default:
			$s_ret='Cmd "'.$cmd.'" is not a valid cmd.';
			break;
	}
}else{
	$s_ret='No cmd sent.';
}
?>