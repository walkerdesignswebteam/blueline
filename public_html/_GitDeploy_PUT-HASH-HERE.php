<?php
require '/_walkersystems/scripts/GitDeploy.class.php';
new GitDeploy(array(
	'bare_repo_path'=>'/home/<useraccount>/mirror.git',
	'web_root_path'=>'/home/<useraccount>/public_html',
	'deploy_branch'=>'master',
	//If you want more debug info in the log. Add this.
	//'debug_mode'=>true,
));